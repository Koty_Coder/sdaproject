package com.websystique.springmvc.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.websystique.springmvc.model.Chat;
import com.websystique.springmvc.model.User;
import com.websystique.springmvc.service.UserService;

@Component
public class FormValidator implements Validator {

	static final Logger logger = LoggerFactory.getLogger(FormValidator.class);

	private Pattern pattern;
	private Matcher matcher;

	@Autowired
	UserService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz) || Chat.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		User user = (User) target;

		String first = user.getFirstName();
		String last = user.getLastName();
		String email = user.getEmail();
		String pass = user.getPassword();
		String confpass = user.getRetypePassword();

		final String request = "^[a-zA-Z]*$";
		final String regexPass = "^[a-zA-Z0-9]*$";
		final String regexemail = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.Firstname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.Lastname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.Email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.Password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retypePassword", "NotEmpty.Retypepasswordd");

		if (!email.isEmpty()) {
			pattern = Pattern.compile(regexemail);
			matcher = pattern.matcher(email);
			if (!matcher.matches()) {
				errors.rejectValue("email", "Nomatch.Email");
			}
		}

		if (pass.length() < 5 || pass.length() > 20) {
			errors.rejectValue("password", "Size.password");
			errors.rejectValue("retypePassword", "Size.password");

		}
		if (confpass.length() < 5 || confpass.length() > 20) {
			errors.rejectValue("password", "Size.password");
			errors.rejectValue("retypePassword", "Size.password");

		}
		if (!pass.equals(confpass)) {
			errors.rejectValue("retypePassword", "Nomatch.Password");
		}
		if (!(pass.isEmpty() && confpass.isEmpty())) {
			pattern = Pattern.compile(regexPass);
			matcher = pattern.matcher(pass);
			if (matcher.matches()) {
				errors.rejectValue("password", "Nomatch.letters.password");
				errors.rejectValue("retypePassword", "Nomatch.letters.password");
			}
		}

		if (first.length() < 5 || first.length() > 20) {
			errors.rejectValue("firstName", "Nomatch.Firstname");
		}

		if (!first.isEmpty()) {
			pattern = Pattern.compile(request);
			matcher = pattern.matcher(first);
			if (!matcher.matches()) {
				errors.rejectValue("firstName", "Nomatch.letters");
			}
		}

		if (last.length() < 5 || last.length() > 20) {
			errors.rejectValue("firstName", "Nomatch.Lastname");
		}

		if (!last.isEmpty()) {
			pattern = Pattern.compile(request);
			matcher = pattern.matcher(last);
			if (!matcher.matches()) {
				errors.rejectValue("lastName", "Nomatch.letters");
			}
		}

	}

}

package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.websystique.springmvc.dao.ChatDao;
import com.websystique.springmvc.dao.UserDao;
import com.websystique.springmvc.model.Chat;
import com.websystique.springmvc.model.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Autowired
	private ChatDao daoc;

	public List<User> findAllUser() { return dao.findAllUser(); }

	public List<Chat> takeMessage() { return daoc.takeMessage(); }

	@Override
	public User findById(int id) { 	return dao.findById(id); }

	@Override
	public void saveUser(User user) { dao.saveUser(user); }

	@Override
	public User findByUsername(String firstName) {
		return dao.findByUsername(firstName);
	}

	@Override
	public boolean isUserEmailUnique(Integer id, String email) {
		User user = findByEmail(email);
		return (user == null || ((id != null) && user.getEmail() == email));
	}

	@Override
	public User findByEmail(String email) { return dao.findByEmail(email); }

	public UserDao getUserDao() { return this.dao; }

	public void setUserDao(UserDao userDao) { this.dao = userDao; }

	@Override
	public User findByPass(String password) { return dao.findByPass(password); }

	@Override
	public boolean isPasswordExist(Integer id, String password) {
		User user = findByPass(password);
		return (user == null || ((id != null) && user.getPassword() == password));
	}

}

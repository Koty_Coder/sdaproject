package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.model.Chat;
import com.websystique.springmvc.model.User;

public interface UserService {

	User findById(int id);

	User findByUsername(String firstName);

	User findByEmail(String email);

	User findByPass(String password);

	void saveUser(User user);

	List<User> findAllUser();

	boolean isUserEmailUnique(Integer id, String email);

	boolean isPasswordExist(Integer id, String password);

	List<Chat> takeMessage();

}
package com.websystique.springmvc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "chat")
public class Chat {

	@Id
	@Column(name = "id")
	private int chatId;

	@Size(max = 255)
	@Column(name = "mess")
	private String message;

	public int getChatId() { return chatId; }

	public void setChatId(int chatId) { this.chatId = chatId; }

	public String getMessage() { return message; }

	public void setMessage(String message) { this.message = message; }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + chatId;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chat other = (Chat) obj;
		if (chatId != other.chatId)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Chat [chatId=" + chatId + ", message=" + message + "]";
	}

}

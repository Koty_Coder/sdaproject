package com.websystique.springmvc.dao;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.Chat;

@Repository("chatDao")
public class ChatDaoImpl extends AbstractDao<Integer, Chat> implements ChatDao {

	@Autowired
	DataSource dataSource;

	public DataSource getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@SuppressWarnings("unchecked")
	public List<Chat> takeMessage() {
		Criteria criteria = createEntityCriteria();
		return (List<Chat>) criteria.list();

	}

}

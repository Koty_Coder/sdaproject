package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.model.Chat;

public interface ChatDao {

	List<Chat> takeMessage();

}

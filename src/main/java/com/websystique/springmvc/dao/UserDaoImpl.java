package com.websystique.springmvc.dao;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@Autowired
	DataSource dataSource;

	public DataSource getDataSource() { return this.dataSource; }

	public void setDataSource(DataSource dataSource) { this.dataSource = dataSource; }

	public User findById(int id) { return getByKey(id); }

	public void saveUser(User user) { persist(user); }

	@SuppressWarnings("unchecked")
	public List<User> findAllUser() {
		Criteria criteria = createEntityCriteria();
		return (List<User>) criteria.list();
	}

	@Override
	public User findByUsername(String firstName) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("firstname", firstName));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findByEmail(String email) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("email", email));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findByPass(String password) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("password", password));
		return (User) criteria.uniqueResult();
	}

}

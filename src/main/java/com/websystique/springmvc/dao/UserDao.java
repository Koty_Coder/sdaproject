package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.model.User;

public interface UserDao {

	User findById(int id);

	User findByUsername(String firstName);

	User findByEmail(String email);

	User findByPass(String password);

	void saveUser(User user);

	List<User> findAllUser();

}

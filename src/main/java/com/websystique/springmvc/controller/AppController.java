package com.websystique.springmvc.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.websystique.springmvc.model.Chat;
import com.websystique.springmvc.model.User;
import com.websystique.springmvc.service.UserService;
import com.websystique.springmvc.validator.FormValidator;

@Controller
public class AppController {

	@Autowired
	UserService service;

	@Autowired
	FormValidator formValid;

	@Autowired
	MessageSource messageSource;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(formValid);
	}

	@RequestMapping(value = { "/index", "/" }, method = RequestMethod.GET)
	public String home(ModelMap mdel) {
		return "index";
	}

	@RequestMapping(value = { "/photo" }, method = RequestMethod.GET)
	public String photo(ModelMap mdel) {
		return "photo";
	}

	@RequestMapping(value = { "/contact" }, method = RequestMethod.GET)
	public String contact(ModelMap model) {
		return "contact";
	}

	@RequestMapping(value = { "/success" }, method = RequestMethod.GET)
	public String success(ModelMap model) {
		return "success";
	}

	@RequestMapping(value = { "/chat" }, method = RequestMethod.GET)
	public String chat(ModelMap model) {
		List<Chat> chat = service.takeMessage();
		model.addAttribute("chat", chat);
		return "chat";
	}

	@RequestMapping(value = { "/cvcard" }, method = RequestMethod.GET)
	public String cvcard(ModelMap model) {
		return "cvcard";
	}

	@RequestMapping(value = { "/userlist" }, method = RequestMethod.GET)
	public String userlist(ModelMap model) {
		List<User> user = service.findAllUser();
		model.addAttribute("user", user);
		return "userlist";
	}

	@RequestMapping(value = { "/worklist" }, method = RequestMethod.GET)
	public String worklist(ModelMap model) {
		return "worklist";
	}

	@RequestMapping(value = { "/aboutme" }, method = RequestMethod.GET)
	public String aboutMe(ModelMap mdel) {
		return "aboutme";
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		return "login";
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.POST)
	public String loginForm(@ModelAttribute(value = "user") User user, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "login";
		}

		if (service.isUserEmailUnique(user.getId(), user.getEmail())) {
			FieldError emailError = new FieldError("user", "email",
					messageSource.getMessage("Nomatch.email", new String[] { user.getEmail() }, Locale.getDefault()));
			result.addError(emailError);
			return "login";
		}
		if (service.isPasswordExist(user.getId(), user.getPassword())) {
			FieldError passError = new FieldError("user", "password", messageSource.getMessage("Nomatch.password",
					new String[] { user.getPassword() }, Locale.getDefault()));
			result.addError(passError);
			return "login";
		}

		model.addAttribute("loginSuccess", " Welcome " + user.getEmail() + " You are logged in");
		return "loginsuccess";
	}

	@RequestMapping(value = { "/register" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("messageFake", "Please used fake email for tests");
		return "register";
	}

	@RequestMapping(value = { "/register" }, method = RequestMethod.POST)
	public String saveUser(@ModelAttribute(value = "user") @Validated @Valid User user, BindingResult result,
			ModelMap model) {
		if (result.hasErrors()) {
			return "register";
		}
		if (!service.isUserEmailUnique(user.getId(), user.getEmail())) {
			FieldError emailError = new FieldError("user", "email", messageSource.getMessage("non.unique.email",
					new String[] { user.getEmail() }, Locale.getDefault()));
			result.addError(emailError);
			return "register";
		}

		service.saveUser(user);
		model.addAttribute("success", " Welcome " + user.getFirstName() + " in my page, I hope you enjoy.");
		return "success";
	}

}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html SYSTEM "about:legacy-compat">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Koty_Code_Aleksander Gustawski</title>
<meta name="description" content="Interactive web site">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="resources/css/ie.css" media="screen, projection"
	rel="stylesheet" type="text/css" />
<link href="resources/css/print.css" media="print" rel="stylesheet"
	type="text/css" />
<!-- Zdalny bootstrap bootstrap.min.css -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Lokalny bootstrap bootstrap.min.css -->
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js "
	type="text/javascript">
	
</script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"
	type="text/javascript"></script>
<!-- Zdalny JQuery -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Zdalny bootstrap.min.js -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Opcjonalnie Zdalny boostrap.theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<!-- Lokalny Stylesheets.screen -->
<link href="resources/css/screen.css" media="screen, projection"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="header">
		<h1>Koty Code</h1>
		<a href="index"> <img class="logo"
			src="resources/images/blackcat.jpg" alt="Cat logo" /></a>
	</div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">MENU <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="login">Login</a></li>
						<li><a href="register">Register</a></li>
						<li><a href="userlist">User List</a></li>
					</ul></li>
				<li class="active"><a href="index">Home</a></li>
				<li><a href="worklist">Work List</a></li>
				<li><a href="chat">Chat</a></li>
				<li><a href="cvcard">Cv-Card</a></li>
				<li><a href="photo">Photo"</a></li>
				<li><a href="aboutme">About me</a></li>
				<li><a href="contact">Contact</a></li>
			</ul>
		</div>
	</nav>
	         <div class="container">
            <h2 class="cvdes"> Please log in</h2>
   <div class="container-i">
    <div class="inner">
        <h1 class="login-name">Koty_Code</h1>
        <h3 class="login-name">Welcome to my web-site</h3>
        
        <c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">�</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>
        <form:form method="POST" modelAttribute="user">
        <form role="form">
        <br>
            <label><span class="glyphicon glyphicon-envelope"></span></label>
            <form:input type="email" placeholder="Email Address" class="input" required="true" name="email" id="email" path="email" /><br>
            <form:errors path="email" cssClass="error" class="error" /><br>
            
            
            <label><span class="glyphicon glyphicon-lock"></span></label>
            <form:input type="password" placeholder="Password" class="input" required="true"  name="password" id="password" path="password" /><br>
            <form:errors path="password" cssClass="error" class="error" /><br>
            
            
            <input type="submit" class="button" value="Login"  />
        </form>
        </form:form>
    </div>    
</div>
    
 
     </div>
    </body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html SYSTEM "about:legacy-compat">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Koty_Code_Aleksander Gustawski</title>
<meta name="description" content="Interactive web site">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="resources/css/ie.css" media="screen, projection"
	rel="stylesheet" type="text/css" />
<link href="resources/css/print.css" media="print" rel="stylesheet"
	type="text/css" />
<!-- Zdalny bootstrap bootstrap.min.css -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Lokalny bootstrap bootstrap.min.css -->
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js "
	type="text/javascript">
	
</script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"
	type="text/javascript"></script>
<!-- Zdalny JQuery -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
<!-- Zdalny bootstrap.min.js -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Opcjonalnie Zdalny boostrap.theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<!-- Lokalny Stylesheets.screen -->
<link href="resources/css/screen.css" media="screen, projection"
	rel="stylesheet" type="text/css" />
	
	        
	 
</head>
<body>
	<div class="header">
		<h1>Koty Code</h1>
		<a href="index"> <img class="logo"
			src="resources/images/blackcat.jpg" alt="Cat logo" /></a>
	</div>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">MENU <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="login">Login</a></li>
						<li><a href="register">Register</a></li>
						<li><a href="userlist">User List</a></li>
					</ul>
				<li class="active"><a href="index">Home</a></li>
				<li><a href="worklist">Work List</a></li>
				<li><a href="chat">Chat</a></li>
				<li><a href="cvcard">Cv-Card</a></li>
				<li><a href="photo">Photo</a></li>
				<li><a href="aboutme">About me</a></li>
				<li><a href="contact">Contact</a></li>
			</ul>
		</div>
	</nav>
	        <div class="container">
            <h2 class="cvdes">Photo Session</h2>
            <div class="col-md-12 hidden-sm hidden-xs" id="slider-thumbs">
                <ul class="list-inline">
                <li> <a id="carousel-selector-0" class="selected">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-1">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-2">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-3">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-4">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-5">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-6">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                <li> <a id="carousel-selector-7">
                <img src="resources/images/minicat.jpg" class="img-responsive">
                </a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12" id="slider">
                <div class="col-md-12" id="carousel-bounding-box">
                    <div id="myCarousel" class="carousel slide">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">
                            <div class="active item" data-slide-number="0">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="1">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="2">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="3">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="4">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="5">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="6">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="7">
                <img src="resources/images/maxcat.jpg" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <script type="text/javascript">
        $('#myCarousel').carousel({
        	interval : 4000
        });
        $('[id^=carousel-selector-]').click(function() {
        	var id_selector = $(this).attr("id");
        	var id = id_selector.substr(id_selector.length - 1);
        	id = parseInt(id);
        	$('#myCarousel').carousel(id);
        	$('[id^=carousel-selector-]').removeClass('selected');
        	$(this).addClass('selected');
        });
        $('#myCarousel').on('slid', function(e) {
        	var id = $('.item.active').data('slide-number');
        	id = parseInt(id);
        	$('[id^=carousel-selector-]').removeClass('selected');
        	$('[id=carousel-selector-' + id + ']').addClass('selected');
        });

        </script>
        </div>

    </body>
</html>